To run current application, execute:


go get bitbucket.org/haluz/bitcoinfrenzy  
go get github.com/tools/godep  
cd $GOPATH/src/bitbucket.org/haluz/bitcoinfrenzy/  
godep go run main.go  

To run tests, execute:

godep go test


You'll get server running on http://localhost:8080