#!/bin/bash
cd webapp
gulp build
cd ..
bee pack -exr="^(?:webapp|tests)$"
rm -rf dist/ && mkdir dist
mv bitcoinfrenzy.tar.gz dist/bitcoinfrenzy.tar.gz