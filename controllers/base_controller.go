package controllers

import (
	"net/http"
	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
	"encoding/json"
	"bitbucket.org/haluz/bitcoinfrenzy/services"
)

type WebSocketController struct {
	beego.Controller
}

func (this *WebSocketController) upgradeToWebsockets() (*websocket.Conn, error) {
	upgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true // to be able to run from localhost:3000
		},
		ReadBufferSize: 1024, WriteBufferSize: 1024}
	ws, err := upgrader.Upgrade(this.Ctx.ResponseWriter, this.Ctx.Request, nil)

	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(this.Ctx.ResponseWriter, "Not a websocket handshake", 400)
		return nil, err
	} else if err != nil {
		beego.Error("Cannot setup WebSocketController connection:", err)
		return nil, err
	}

	return ws, nil
}

// here we send data to client and on disconnect send request to remove subscription
func (this *WebSocketController) startSendData(ws *websocket.Conn, service *services.PriceServiceImpl, listenerChannel chan services.PriceData) {
	for {
		data, more := <-listenerChannel // read while channel is not closed
		if more {
			b, err := json.Marshal(data)
			if err != nil {
				service.RemoveSubscription(listenerChannel)
			}
			if ws.WriteMessage(websocket.TextMessage, b) != nil {
				// disconnected, remove subscription
				service.RemoveSubscription(listenerChannel)
			}
		} else {
			return
		}
	}
}
