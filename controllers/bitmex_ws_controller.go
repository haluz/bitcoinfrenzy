package controllers

import (
	"bitbucket.org/haluz/bitcoinfrenzy/services/bitmex"
)

type BitMexWebSocketController struct {
	WebSocketController
}

func (this *BitMexWebSocketController) Connect() {
	ws, err := this.upgradeToWebsockets()
	if err != nil {
		return
	}
	defer ws.Close()

	service := bitmex.GetBitMexService()
	priceChan := service.Subscribe()

	this.startSendData(ws, service.PriceServiceImpl, priceChan)
}
