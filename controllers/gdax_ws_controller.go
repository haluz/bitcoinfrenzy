package controllers

import (
	"bitbucket.org/haluz/bitcoinfrenzy/services/gdax"
)

type GdaxWebSocketController struct {
	WebSocketController
}

func (this *GdaxWebSocketController) Connect() {
	ws, err := this.upgradeToWebsockets()
	if err != nil {
		return
	}
	defer ws.Close()

	service := gdax.GetGdaxService()
	priceChan := service.Subscribe()

	this.startSendData(ws, service.PriceServiceImpl, priceChan)
}
