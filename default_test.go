package main

import (
	"testing"
	_ "bitbucket.org/haluz/bitcoinfrenzy/routers"

	"github.com/astaxie/beego"
	"log"
	ws "github.com/gorilla/websocket"
	"time"
	"bitbucket.org/haluz/bitcoinfrenzy/services"
)

func init() {
	go beego.Run(":15987")
}

func TestGdax(t *testing.T) {
	time.Sleep(time.Second) // let server run

	var wsDialer ws.Dialer
	c, _, err := wsDialer.Dial("ws://localhost:15987/gdaxws/connect", nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	message := services.PriceData{}
	if err := c.ReadJSON(&message); err != nil {
		beego.Error(err.Error())
		return
	}

	if(message.Price == 0) {
		t.Fail()
	}
}

func TestBitmex(t *testing.T) {
	time.Sleep(time.Second) // let server run

	var wsDialer ws.Dialer
	c, _, err := wsDialer.Dial("ws://localhost:15987/bitmexws/connect", nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	message := services.PriceData{}
	if err := c.ReadJSON(&message); err != nil {
		beego.Error(err.Error())
		return
	}

	if(message.Price == 0) {
		t.Fail()
	}
}