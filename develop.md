# Backend

To be able to build you need to install next packages:

```
go get -u github.com/astaxie/beego
go get -u github.com/beego/bee
go get github.com/gorilla/websocket
go get github.com/preichenberger/go-gdax
```

Run next 3 commands to pack application into archive:

```
bee pack -exr="^(?:webapp|tests)$"
rm -rf dist/ && mkdir dist
mv bitcoinfrenzy.tar.gz dist/bitcoinfrenzy.tar.gz
```

As result you will have archive with executable for you platform in ```dist/bitcoinfrenzy.tar.gz```.

# Frontend

Run from root:

```apple js
cd webapp
npm install
bower install
gulp serve
```

Run next command to build UI part:

```apple js
gulp build
```