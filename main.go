package main

import (
	_ "bitbucket.org/haluz/bitcoinfrenzy/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Info("Open http://localhost:8080")
	beego.Run()
}

