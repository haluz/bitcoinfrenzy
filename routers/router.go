package routers

import (
	"github.com/astaxie/beego"
	"bitbucket.org/haluz/bitcoinfrenzy/controllers"
)

func init() {
	beego.DelStaticPath("/static")
	beego.SetStaticPath("/", "webdist/index.html")
	beego.SetStaticPath("/assets", "webdist/assets")
	beego.SetStaticPath("/fonts", "webdist/fonts")
	beego.SetStaticPath("/maps", "webdist/maps")
	beego.SetStaticPath("/scripts", "webdist/scripts")
	beego.SetStaticPath("/styles", "webdist/styles")

	// websocket
	beego.Router("/gdaxws/connect", &controllers.GdaxWebSocketController{}, "get:Connect")
	beego.Router("/bitmexws/connect", &controllers.BitMexWebSocketController{}, "get:Connect")
}
