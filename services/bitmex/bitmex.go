package bitmex

import (
	ws "github.com/gorilla/websocket"
	"github.com/astaxie/beego"
	"bitbucket.org/haluz/bitcoinfrenzy/services"
)

type BitMexService struct {
	*services.PriceServiceImpl
}

var service *BitMexService

func GetBitMexService() *BitMexService {
	return service
}

func init() {
	service = &BitMexService{PriceServiceImpl: services.NewPriceServiceImpl()}
	go service.startReceivingData(3)
	beego.Info("BITMEX service started")
}

func (this *BitMexService) startReceivingData(reconnectTries int) {
	var wsDialer ws.Dialer
	conn, _, err := wsDialer.Dial("wss://www.bitmex.com/realtime?subscribe=instrument:XBTUSD", nil)
	if err != nil {
		beego.Error(err.Error())
		panic("BITMEX is not available")
	}
	defer conn.Close()

	var dataMessage []byte

	welcome := WelcomeResponse{}
	dataMessage, err = services.ReadMessage(conn, &welcome)
	if err != nil {
		this.tryReconnectService(dataMessage, reconnectTries)
		return
	}

	isSubscribeSuccess := SubscribeResponse{}
	dataMessage, err = services.ReadMessage(conn, &isSubscribeSuccess)
	if err != nil || !isSubscribeSuccess.Success {
		// sometimes after server restart bitmex doesn't send welcome messages and starts to send asked data immediately
		this.tryReconnectService(dataMessage, reconnectTries)
		return
	}

	for true {
		instrumentResponse := InstrumentTableResponse{}
		dataMessage, err = services.ReadMessage(conn, &instrumentResponse)
		if err != nil {
			this.tryReconnectService(dataMessage, reconnectTries)
			return
		}

		data := instrumentResponse.Data[0]
		if data.LastPrice != 0.0 { // sometimes there is a data with null values
			data := services.PriceData{Time: data.Timestamp, Price: data.LastPrice}
			this.Notify(data)
		}
	}
}

func (this *BitMexService) tryReconnectService(message []byte, reconnectTries int) {
	if reconnectTries < 0 {
		panic("Can't start BITMEX service. Shutdown.")
	}

	beego.Info("Got problems with message: " + string(message))
	beego.Info("Restart BITMEX service")
	go this.startReceivingData(reconnectTries-1)
}
