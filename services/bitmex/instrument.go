package bitmex

import (
	"time"
)

type Instrument struct {
	LastPrice float64   `json:"lastPrice,omitempty"`
	Timestamp time.Time `json:"timestamp"`
}
