package bitmex

type InstrumentTableResponse struct {
	Table string `json:"table"`
	Data  []Instrument `json:"data"`
}