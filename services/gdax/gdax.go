package gdax

import (
	"github.com/preichenberger/go-gdax"
	"github.com/astaxie/beego"
	ws "github.com/gorilla/websocket"
	"bitbucket.org/haluz/bitcoinfrenzy/services"
)

type GdaxService struct {
	secret     string
	key        string
	passphrase string

	*services.PriceServiceImpl
}

var service *GdaxService

func init() {
	service = &GdaxService{secret: "secret", key: "key", passphrase: "pass", PriceServiceImpl: services.NewPriceServiceImpl()}
	go service.startReceivingData(3)
	beego.Info("GDAX service started")
}

func GetGdaxService() *GdaxService {
	return service
}

func (this *GdaxService) startReceivingData(reconnectTries int) {
	var wsDialer ws.Dialer
	wsConn, _, err := wsDialer.Dial("wss://ws-feed.gdax.com", nil)
	if err != nil {
		beego.Error(err.Error())
		panic("GDAX is not available")
	}

	err = subscribeToChannel(wsConn)
	if err != nil {
		this.tryReconnectService(reconnectTries-1)
		return
	}

	message := gdax.Message{}
	for true {
		if err := wsConn.ReadJSON(&message); err != nil {
			beego.Error(err.Error())
			this.tryReconnectService(reconnectTries-1)
			return
		}

		if message.Type == "ticker" && message.Price != 0.0 {
			data := services.PriceData{Price: message.Price, Time: message.Time.Time()}
			this.Notify(data)
		}
	}
}

func subscribeToChannel(wsConn *ws.Conn) error {
	subscribe := gdax.Message{
		Type: "subscribe",
		Channels: []gdax.MessageChannel{
			{
				Name: "ticker",
				ProductIds: []string{
					"BTC-USD",
				},
			},
		},
	}
	return wsConn.WriteJSON(subscribe)
}

func (this *GdaxService) tryReconnectService(reconnectTries int) {
	if reconnectTries < 0 {
		panic("Can't start GDAX service. Shutdown.")
	}

	beego.Info("Restart GDAX service")
	go this.startReceivingData(reconnectTries-1)
}
