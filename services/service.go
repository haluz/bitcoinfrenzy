package services

import (
	"time"
	"sync"
	"github.com/astaxie/beego"
	"container/list"
	"encoding/json"
	"github.com/gorilla/websocket"
)

/*
Common code for price services(gdax, bitmex) here.

Client can subscribe to PriceService and get a channel to get prices constantly. When client, disconnects PriceService.RemoveSubscription
method is called. It pushes an intention to remove listened channel from service to PriceServiceImpl.listenersToRemove linked list.
Later on new message receive, these listener channels will be removed. This deferred removal is done intentionally,
so we don't get a deadlock (writer thread is that which should close the channel it writes to).

The idea was that we don't want a new connection to gdax.com or bitmex.com on every new client (browser). Instead,
there is one singleton price service, which has listener channels (for clients/browsers). On new message arrived from
gdax/bitmex it iterates through listener channels and sends this message to each of them.
 */

type PriceData struct {
	Price float64   `json:"price,string"`
	Time  time.Time `json:"time,string"`
}

type PriceService interface {
	Subscribe() chan PriceData
	RemoveSubscription(l chan PriceData)
	startReceivingData()
}

type PriceServiceImpl struct {
	Listeners         []chan PriceData
	Mutex             *sync.Mutex
	listenersToRemove  *list.List
}

func NewPriceServiceImpl() *PriceServiceImpl {
	return &PriceServiceImpl{Mutex : &sync.Mutex{}, listenersToRemove : list.New()}
}

func (this *PriceServiceImpl) Subscribe() chan PriceData {
	this.Mutex.Lock()
	defer this.Mutex.Unlock()

	beego.Info("Client subscribed")
	l := make(chan PriceData)

	this.Listeners = append(this.Listeners, l)
	return l
}

func (this *PriceServiceImpl) RemoveSubscription(l chan PriceData) {
	// run as a new thread, so we don't block the current if Mutex is locked at the moment
	go func() {
		this.Mutex.Lock()
		defer this.Mutex.Unlock()
		this.listenersToRemove.PushFront(l)
	}()
}

func (this *PriceServiceImpl) Notify(data PriceData) {
	this.Mutex.Lock()
	defer this.Mutex.Unlock()

	this.removeMarkedListeners() // remove listeners marked as deleted first

	for _, listener := range this.Listeners {
		listener <- data
	}
}

func (this *PriceServiceImpl) removeMarkedListeners() {
	for e := this.listenersToRemove.Front(); e != nil; e = e.Next() {
		this.removeListener(e.Value.(chan PriceData))
	}

	// clear the list
	this.listenersToRemove = list.New()
}

func (this *PriceServiceImpl) removeListener(l chan PriceData) {
	for i, v := range this.Listeners {
		if v == l {
			this.Listeners = append(this.Listeners[:i], this.Listeners[i+1:]...)
			close(l)
			beego.Info("Client Unsubscribed")
			return
		}
	}
}

func ReadMessage(conn *websocket.Conn, v interface{}) ([]byte, error) {
	_, message, err := conn.ReadMessage()
	if err != nil {
		beego.Error("read:", err)
		return nil, err
	}

	json.Unmarshal(message, v)
	return message, nil
}
