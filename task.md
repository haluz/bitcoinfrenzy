Hi Mykola -

Can you ask Eugene whether he wants to do this small task? Also what is his notice time? A.k.a when he can join us.

Bitcoin Frenzy
Bitcoin is going crazy. People are trading Bitcoin and its derivatives. Can you use Golang to write a program to:
- Get real-time BTC's price from GDAX API
- Get real-time future XBTUSD's price from BitMex API
- Visualize the two price trends on the same graph
- If BTC price on GDAX is a consistent leading factor for XBTUSD's price on BitMex, there is a chance to write a bot to do automatic trading.

For the visualization - you can do web UI, or native Go UI, e.g.:
https://github.com/avelino/awesome-go#gui
https://github.com/gizak/termui

So the only requirement is that the main BE code is Go. Other than that you can choose any technologies you wish.


