(function() {
  'use strict';

  angular
    .module('webapp', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr',
      "chart.js", "ngWebSocket", "nvd3"]);
})();
