(function () {
  'use strict';

  angular
    .module('webapp')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, GdaxService, BitMexService) {
    $scope.options = {
      chart: {
        type: 'lineChart',
        height: 650,
        margin: {
          top: 20,
          right: 20,
          bottom: 60,
          left: 65
        },
        x: function (d) {
          if(d == undefined) {
            return undefined;
          }
          return d[0];
        },
        y: function (d) {
          if(d == undefined) {
            return undefined;
          }
          return d[1];
        },
        average: function (d) {
          return undfined;
        },

        color: d3.scale.category10().range(),
        duration: 500,
        useInteractiveGuideline: true,
        clipVoronoi: false,

        xAxis: {
          axisLabel: 'Time',
          tickFormat: function (d) {
            var date = new Date(d);
            var formatTime = d3.time.format("%H:%M:%S");
            var str = formatTime(date);
            return str;
          },
          showMaxMin: true,
          staggerLabels: true
        },

        yAxis: {
          axisLabel: 'Price',

          tickFormat: function (d) {
            return d3.format(',.1')(d);
          },
          axisLabelDistance: 100
        }
      }
    };

    $scope.data = [
      {
        key: "GDAX",
        values: []
      },
      {
        key: "BITMEX",
        values: []
      }
    ];

    GdaxService.addListener(function (message) {
      var prices = $scope.data[0];
      var graphPoint = parsePriceMessage(message);
      prices.values.push(graphPoint);
      $scope.gdaxPrice = graphPoint[1];
    });

    BitMexService.addListener(function (message) {
      var prices = $scope.data[1];
      var graphPoint = parsePriceMessage(message);
      prices.values.push(graphPoint);
      $scope.bitmexPrice = graphPoint[1];
    });

    function parsePriceMessage(message) {
      var time = Date.parse(message.time);
      var price = parseFloat(message.price);

      return [new Date(time), price];
    }
  }
})();
