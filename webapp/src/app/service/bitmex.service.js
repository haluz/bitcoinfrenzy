angular.module('webapp')
  .factory('BitMexService', function ($websocket) {
    // Open a WebSocket connection
    var dataStream = $websocket('ws://localhost:8080/bitmexws/connect');

    var listeners = [];

    dataStream.onMessage(function (message) {
      angular.forEach(listeners, function(listener) {
        listener.call(listener, JSON.parse(message.data));
      });
    });

    var methods = {
      addListener: function(listener) {
        listeners.push(listener);
      }
    };

    return methods;
  });
